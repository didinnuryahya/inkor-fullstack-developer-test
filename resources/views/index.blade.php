<!DOCTYPE html>
<html>
<head>
    <title>Computer Based Test</title>
</head>
<body>
    <h1>Computer Based Test</h1>

    <form method="POST" action="{{ route('cbt.result') }}">
        @csrf
        @foreach ($soals as $soal)
            <p>{{ $soal->pertanyaan }}</p>
            @foreach ($soal->jawabans as $jawaban)
                <label>
                    <input type="radio" name="jawaban[{{ $soal->id }}]" value="{{ $jawaban->id }}">
                    {{ $jawaban->jawaban }}
                </label><br>
            @endforeach
        @endforeach
        <button type="submit">Submit</button>
    </form>
    <!-- Tambahkan ini setelah </form> -->
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</body>
</html>
