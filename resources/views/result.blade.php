<!DOCTYPE html>
<html>
<head>
    <title>Hasil Akhir CBT</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <h1>Hasil Akhir CBT</h1>
    <p>Nilai Akhir: {{ $nilaiAkhir }}%</p>

    <!-- Tampilkan spider chart di sini -->
    <canvas id="spiderChart"></canvas>

    <script>
        // Data untuk spider chart
        var dataSpiderChart = @json($dataSpiderChart);

        // Buat spider chart
        var ctx = document.getElementById('spiderChart').getContext('2d');
        var spiderChart = new Chart(ctx, {
            type: 'radar',
            data: {
                labels: Object.keys(dataSpiderChart),
                datasets: [{
                    label: 'Persentase Jawaban',
                    data: Object.values(dataSpiderChart),
                    borderColor: 'rgba(75, 192, 192, 1)',
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                }]
            },
            options: {
                scale: {
                    ticks: {
                        beginAtZero: true,
                        max: 100,
                        stepSize: 20,
                    }
                }
            }
        });
    </script>

</body>
</html>
