<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CBTController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [CBTController::class, 'index'])->name('cbt.index');
Route::post('/result', [CBTController::class, 'showResult'])->name('cbt.result');

