<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Soal;
use App\Models\Jawaban;

class CBTController extends Controller
{
    public function index()
    {
        $soals = Soal::with('jawabans')->get();

        return view('index', compact('soals'));
    }

    public function showResult(Request $request)
    {
        $jawabanUser = $request->input('jawaban');
        $totalSoal = count($jawabanUser);
        $totalBenar = 0;

        // Data dummy, gantilah dengan data yang sesuai dari database
        $jawabanBenar = [
            1 => 2, // Jawaban benar untuk Soal 1 adalah pilihan 2
            2 => 1, // Jawaban benar untuk Soal 2 adalah pilihan 1
            // Lanjutkan sesuai dengan banyaknya soal
        ];

        foreach ($jawabanUser as $soalId => $jawabanId) {
            $jawaban = Jawaban::find($jawabanId);
            if ($jawaban && $jawaban->soal_id == $soalId && $jawaban->benar) {
                $totalBenar++;
            }
        }

        $nilaiAkhir = ($totalBenar / $totalSoal) * 100;

        // Data untuk spider chart
        $dataSpiderChart = [
            'Benar' => $nilaiAkhir,
            'Salah' => 100 - $nilaiAkhir,
        ];

        return view('result', compact('nilaiAkhir', 'dataSpiderChart'));
    }

}
