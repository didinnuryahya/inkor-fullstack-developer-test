<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Soal;
use App\Models\Jawaban;

class SoalJawabanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Tambahkan data dummy untuk Soal dan Jawaban
        $soal1 = Soal::create(['pertanyaan' => 'Keanekaragaman dalam masyarakat menjadi tantangan tersendiri, apalagi jika tumbuhnya perasaan kedaerahan dan kesukuan yang berlebihan bisa saja mengancam keutuhan bangsa dan Negara Kesatuan Republik Indonesia.Sebuah bentuk usaha untuk meningkatkan perdamaian antar suku, pemeluk agama, serta golongan sosial lainnya bisa dilakukan melalui dialog dan kerja sama dengan prinsip']);
        $soal2 = Soal::create(['pertanyaan' => 'Pancasila sila ketiga berbunyi']);
        $soal3 = Soal::create(['pertanyaan' => 'Pada peringatan Hari Kartini, semua siswa kelas 7 akan menggunakan pakaian adat. Melati akan menggunakan pakaian adat tradisional dari Betawi yang khas dengan kebaya dan selendang kepala, sedangkan Husna akan menggunakan pakaian adat dari Makasar, yaitu baju bodo lengkap dengan perhiasannya. Pakaian-pakaian adat yang akan dikenakan oleh siswa kelas 7 merupakan bagian dari']);
        $soal4 = Soal::create(['pertanyaan' => 'Bunga mempunyai kulit seperti sawo yang matang, tubuh tinggi, mata besar dan berwarna cokelat muda, sedangkan Jelita mempunyai kulit kuning langsat, berambut lurus, mata sipit dan berwarna cokelat tua. Perbedaan dari ciri-ciri fisik antara Bunga dan Jelita tersebut disebut …..']);
        $soal5 = Soal::create(['pertanyaan' => 'Negara Indonesia yang beragam suku, bangsa, budaya, agama, ras, dan antar golongan jikalau dikelola dengan baik maka dapat']);
        $soal6 = Soal::create(['pertanyaan' => 'Semboyan Bhinneka Tunggal Ika memiliki makna, walaupun beragam……']);
        $soal7 = Soal::create(['pertanyaan' => ' Negara Indonesia terdiri dari beberapa wilayah yang sangat luas terbentang dari Sabang sampai Merauke. Indonesia juga mempunyai ribuan pulau yang tersebar dan menjadi tempat tinggalnya penduduk dengan ragam suku bangsa, bahasa, budaya, agama dan adat istiadat. Arti penting dari keberagaman suku, ras, agama dan antar golongan bagi bangsa Indonesia yaitu salah satunya ……']);
        $soal8 = Soal::create(['pertanyaan' => ' Si Entong merupakan pelajar di sekolah favorit, yang mayoritas siswanya berasal dari keluarga yang mampu. Lalu Si Entong terpilih menjadi ketua kelas, padahal Si Entong sendiri adalah siswa yang berasal dari keluarga tidak mampu.Dari cerita cerita singkat di atas, menggambarkan bahwa di sekolah Si Entong telah menjunjung nilai-nilai toleransi dalam keberagaman, yaitu toleransi antar']);
        $soal9 = Soal::create(['pertanyaan' => 'Kolaborasi dalam mendepak penjajah untuk mencapai kemerdekaan tanpa dibatasi oleh perbedaan suku bangsa, adat istiadat, agama merupakan bukti nyata kemampuan bangsa Indonesia dalam hal ….']);
        $soal10 = Soal::create(['pertanyaan' => 'Tuhan menciptakan perbedaan pada setiap manusia dan perbedaan itu merupakan….']);


        Jawaban::create(['soal_id' => $soal1->id, 'jawaban' => 'A. Kebersamaan, kesetaraan, toleransi, dan saling menghormati', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal1->id, 'jawaban' => 'B. Kesetaraan, kesejajaran, keadilan, dan mufakat.', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal1->id, 'jawaban' => 'C. Musyawarah mufakat, keadilan, dan kemandirian.', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal1->id, 'jawaban' => 'D. Kepercayaan, kedamaian, toleransi, dan kesetaraan.', 'benar' => false]);

        Jawaban::create(['soal_id' => $soal2->id, 'jawaban' => 'A. Ketuhanan yang maha esa', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal2->id, 'jawaban' => 'B. kemanusiaan yang adil dan beradab', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal2->id, 'jawaban' => 'C. persatuan indonesia', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal2->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);
        
        
        Jawaban::create(['soal_id' => $soal3->id, 'jawaban' => 'A. Ras', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal3->id, 'jawaban' => 'B. Suku dan bangsa', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal3->id, 'jawaban' => 'C. Agama', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal3->id, 'jawaban' => 'D. Multikulturalisme', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal4->id, 'jawaban' => 'A. Ras', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal4->id, 'jawaban' => 'B. Budaya', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal4->id, 'jawaban' => 'C. Golongan', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal4->id, 'jawaban' => 'D. Suku Bangsa', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal5->id, 'jawaban' => 'A. Mendorong percepatan pembangunan daerah', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal5->id, 'jawaban' => 'B. Sumber pendapatan di daerah-daerah', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal5->id, 'jawaban' => 'C. Kekayaan bangsa yang sangat berharga', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal5->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal6->id, 'jawaban' => 'A. Suku bangsa, agama, ras dan antar golongan tetapi tetap satu kesatuan', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal6->id, 'jawaban' => 'B. Pemikiran tetapi tetap untuk kemajuan Indonesia', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal6->id, 'jawaban' => 'C. Indonesia negara majemuk, tetapi mampu hidup rukun', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal6->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal7->id, 'jawaban' => 'A. Penyebab konflik antar suku, agama, ras dan antar golongan', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal7->id, 'jawaban' => 'B. Penyebab banyaknya aksi demonstrasi memperjuangkan hak masyarakat', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal7->id, 'jawaban' => 'C. Keberagaman bangsa yang sangat berharga untuk mencapai tujuan', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal7->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal8->id, 'jawaban' => 'A. Umat Beragama', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal8->id, 'jawaban' => 'B. Suku', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal8->id, 'jawaban' => 'C. Golongan', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal8->id, 'jawaban' => 'D. Daerah', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal9->id, 'jawaban' => 'A. Berkompromi dengan penjajah', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal9->id, 'jawaban' => 'B. Mempersatukan perbedaan yang ada', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal9->id, 'jawaban' => 'C. Berdialog dengan penjajah', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal9->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);

        
        Jawaban::create(['soal_id' => $soal10->id, 'jawaban' => 'A. suatu masalah yang wajib kita hindari', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal10->id, 'jawaban' => 'B. suatu masalah yang perlu menjadi perhatian', 'benar' => false]);
        Jawaban::create(['soal_id' => $soal10->id, 'jawaban' => 'C. mengabaikan kepentingan nasional', 'benar' => true]);
        Jawaban::create(['soal_id' => $soal10->id, 'jawaban' => 'D. Salah semua', 'benar' => false]);

        
    }
}
